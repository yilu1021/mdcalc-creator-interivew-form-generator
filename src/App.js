import React, { Component } from 'react';
import Questions from './Components/Questions';
import AddQuestion from './Components/AddQuestion';
import $ from 'jquery';
import './App.css';

class App extends Component {
    constructor() {
        super();
        var initialState = this.initialState()
        initialState.existing = {}
        this.state = initialState

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleImport = this.handleImport.bind(this);
        this.handleDelete = this.handleDelete.bind(this)
    }

    initialState() {
      return {
            form: {
              formName: '',
              formCalc: '',
              formID: '',
              formImage: '',
              formBio: '',
              formQuestions: {
                'Why did you develop the [CALC NAME]?  Was there a particular clinical experience or patient encounter that inspired you to create this tool for clinicians?': '',
                'What pearls, pitfalls and/or tips do you have for users of the [CALC NAME]? Do you know of cases when it has been applied, interpreted, or used inappropriately?': '',
                'What recommendations do you have for doctors once they have applied the [CALC NAME]? Are there any adjustments or updates you would make to the score based on new data or practice changes?': '',
                'How do you use the [CALC NAME] in your own clinical practice? Can you give an example of a scenario in which you use it?': '',
                'Any other research in the pipeline that you’re particularly excited about?': ''
              }
            },
            dbid: null,         //this is the id generated in the DB
            generate: false,     //once posted to DB, will become true, and the generated link will appear
            submitting: false,
            deleting: false
        }
    }

    APIBaseURL() {
        // var loc = window.location.origin;
        // var newurl = "/v1/creatorformapp/";
        // if (loc.indexOf('localhost:3000') != -1){
        //  newurl = "http://localhost:7000/v1/creatorformapp/";
        // }      
      return 'https://api.mdcalc.com'
      // return 'http://localhost:7000'
    }

    MDCalcWebBaseURL() {
        // var loc = window.location.origin;
        // var name = this.state.form.formName;

        // if (loc.indexOf('localhost:3000') != -1){
        //   var link = this.MDCalcWebBaseURL() + "/creatorInterview/";
        // }else if(loc.indexOf("api.clacdm.com") != -1){
        //   var link = this.MDCalcWebBaseURL() + "/creatorInterview/";
        // }else if(loc.indexOf("api.mdcalc.com") != -1){
        //   var link = this.MDCalcWebBaseURL() + "/creatorInterview/";
        // }      
      // return 'http://localhost:8081'
      return 'https://www.mdcalc.com'
    }

    // post to form table in the database to create a form record
    createForms(formData) {
      var apiEndPoint = this.APIBaseURL() + '/v1/creatorformapp/'
      if (this.state.dbid) {
        apiEndPoint += this.state.dbid
        formData={
          name: this.state.form.formName,
          score: this.state.form.formCalc,
          calcID: this.state.form.formID,
          imageAdd: this.state.form.formImage,
          bio: this.state.form.formBio,
          qa: JSON.stringify(this.state.form.formQuestions)
        };
      }

      this.setState({
        submitting: true
      })

      $.ajax({
          type: 'POST',
          url:  apiEndPoint,
          data: formData,
          dataType: 'json',
          cache: false,
          success: function(data) {


            if (data.status !== 200) {
              alert(data.message)
              this.setState({
                submitting: false
              })
            } else {
              this.setState({
                submitting: null
              })
              setTimeout(() => {window.location.reload()}, 1009)
            }

          }.bind(this),
          error: function(xhr, status, err) {
              console.log(err);
              console.log("fail")
          }
      });
    }

    handleSubmit(event) {
        var dictQuest = this.state.form.formQuestions;
        var dictQuest2 = {};
        //replace all the "CALC NAME"s with the inputted Calc name
        for (var quest in dictQuest){
          quest = quest.replace("[CALC NAME]", this.state.form.formCalc);
          dictQuest2[quest] = '';
        }
        //if user did not provide an image, use the MDCalc default one
        var image = this.state.form.formImage;
        if(image === ''){
          image = 'https://cdn-web-img.mdcalc.com/people/default.jpeg';
        }

        if (!this.state.form.formName) {
          alert('creator name is required!')
        } else if (!this.state.form.formCalc) {
          alert('calc name is required!')
        } else if (!this.state.form.formID) {
          alert('calc id is required!')
        } else if (!JSON.stringify(dictQuest2)) {
          alert('form questions is required!')
        } else {
          let data = {
               formName: this.state.form.formName,
               formCalc: this.state.form.formCalc,
               formID: this.state.form.formID,
               formImage: image,
               formBio: this.state.form.formBio,
               formQuestions: JSON.stringify(dictQuest2)
          }
          //calls function to post to DB
          this.createForms(data);          

        }

    }

    componentWillMount() {
      this.getExistingForms();

    }

    componentDidMount() {
    }

    //takes args passed from AddQuestion.js
    handleAddQuestion(question) {
      //grab the questions that are currently stored, append the new question to them, and readd to state
        let questions = this.state.form.formQuestions;
        var key = Object.keys(question)[0];
        questions[key] = "";

        this.setState({
          form : {
            formName: this.state.form.formName,
            formCalc: this.state.form.formCalc,
            formID: this.state.form.formID,
            formImage: this.state.form.formImage,
            formBio: this.state.form.formBio,
            formQuestions: questions
          }
        });

    }

    handleDeleteQuestions(target) {
        //compares by the string of the question
      if(window.confirm('You are about to delete this question: \n\n\"' + target + '\"')){
        var questionsArray = Object.keys(this.state.form.formQuestions);
        var i;
        var questions = {};
        for(i = 0; i < questionsArray.length; i++){
          //add those that aren't the one that was deleted
          if(questionsArray[i] !== target){
              questions[questionsArray[i]] = "";
          }
        }

        this.setState({
          form: {
            formName: this.state.form.formName,
            formCalc: this.state.form.formCalc,
            formID: this.state.form.formID,
            formImage: this.state.form.formImage,
            formBio: this.state.form.formBio,
            formQuestions: questions
          }
        });
      }

    }

    generateFormLink(){

      if(this.state.generate === true){

        var link = this.MDCalcWebBaseURL() + "/creatorInterview/";

        var name = this.state.form.formName;
        var link2 = link + name + "?formID=" + this.state.dbid;

        return(
          <div>
            <a href={link2} className='buttonView' target="_blank">View Creator Form on www.mdcalc.com </a>
          </div>
        )
      }
    }

    changeValue(e) {

        var currentForm = this.state.form;
        //find by attribute name, ex: "formID", "formCalc", "formName", "formImage"
        var id = e.target.getAttribute("name");
        currentForm[id] = e.target.value;
        this.setState({
          form: currentForm
        })

    }

    handleImport(){

      if (!this.state.dbid) {
        return
      }

      var id = this.state.dbid;

      $.ajax({
            type: 'GET',
            url: this.APIBaseURL() + '/v1/creatorformapp/' + id,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({ form:
                    {
                      formName: data.form.name,
                      formCalc: data.form.score,
                      formID: data.form.calcID,
                      formImage: data.form.imageAdd,
                      formBio: data.form.bio,
                      formQuestions: JSON.parse(data.form.qa)
                    },
                    dbid: id,
                    generate: true
                }, function() {
                });
            }.bind(this),
            error: function(xhr, status, err) {
                console.log(err);
            }
        });

    }

    getExistingForms(){

          $.ajax({
              type: 'GET',
              url: this.APIBaseURL() + '/v1/creatorformappids/',
              dataType: 'json',
              cache: false,
              success: function(data) {
                var existing = {}

                data.list.sort(function(a,b) {return (a.score > b.score) ? 1 : ((b.score > a.score) ? -1 : 0);} );

                data.list.forEach(function(form){
                  existing[form.id] = {
                    score_name: form.score,
                    creator_name: form.name
                  }
                })

                this.setState({
                  existing: existing
                });
              }.bind(this),
              error: function(xhr, status, err) {
                  console.log(err);
                  console.log("Did not get existing forms")
              }
          });
    }
    changeSelection(event){

      var form_id = event.target.value;

      if (!form_id) {
        var initialState = this.initialState()
        this.setState(initialState)
      } else {
        this.setState({
          dbid: form_id
        }, this.handleImport);
      }

    }

    renderFormButton(){
      var text = ''
      if (this.state.submitting == true) {
        text = 'Submitting'
      } else if (this.state.submitting == null) {
        text = 'Success! Refreshing the page now'
      } else if (this.state.dbid) {
        text = 'Save'
      } else {
        text = 'Generate Form'
      }

      return(
        <div>
          <button className='buttonSubmit' onClick={this.handleSubmit}>{text}</button>
        </div>
      )
    }

    handleDelete() {

      var link = this.MDCalcWebBaseURL() + "/creatorInterview/" + this.state.form.formName + "?formID=" + this.state.dbid;

      let confirmdelete = window.confirm("Do you really want to delete " + this.state.form.formName + " interview form to " + this.state.existing[this.state.dbid].creator_name + "?" + " Make sure you have not sent " + link + " to be filled out by " + this.state.existing[this.state.dbid].creator_name)
      
      if (confirmdelete) {
                  var apiEndPoint = this.APIBaseURL() + '/v1/creatorformapp/' + this.state.dbid

                  this.setState({
                    deleting: true
                  })

                  $.ajax({
                      type: 'Delete',
                      url:  apiEndPoint,
                      dataType: 'json',
                      cache: false,
                      success: function(data) {

                        if (data.status !== 200) {
                          alert("error deleting, talk to yi")
                          this.setState({
                            deleting: false
                          })
                        } else {
                          this.setState({
                            deleting: null
                          })
                          setTimeout(() => {window.location.reload()}, 1009)
                        }

                      }.bind(this),
                      error: function(xhr, status, err) {
                          console.log(err);
                          console.log("fail")
                      }
                  });
      }
    }

    renderDeleteButton() {
      var text = ""
      if (this.state.deleting) {
        text = 'Deleting'
      } else if (this.state.deleting == null) {
        text = 'Successfully Deleted. Refreshing the page now'
      } else {
        text = "delete"
      }

      if (this.state.dbid) { 
        return(
          <div>
            <button className='buttonSubmit' onClick={this.handleDelete}>{text}</button>
          </div>
        )        
      }
    }

    render() {
       //can reference proper reference in the map function below
      var self = this;
      var existing_form_ids = Object.keys(this.state.existing);
      //names = names.push("Select a form");
        return (
            <div className="App">
              <div className="App-header">
              <img className='logo' src="http://mdcalc-assets.imgix.net/mdcalc_logo_green_square_1024.png"></img>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Creator Interview Form Builder

              </div>
              <br></br><br></br>
            <div className="AppText">
              <div>
                <label>Existing form:&nbsp;</label>

                <select name="dbid" value={this.state.dbid} onChange={self.changeSelection.bind(self)}>
                  <option value=''>Create a new form</option>
                  {
                    existing_form_ids.map(function(form_id){
                      return(
                        <option value={form_id}>{self.state.existing[form_id].score_name + " - " + self.state.existing[form_id].creator_name}</option>
                      )
                    })
                  }
                </select>
              </div>
              &nbsp;&nbsp;&nbsp;
              {this.generateFormLink()}

              <br></br>
              <br></br>
              <div className='title'>Creator information:</div>
              <hr/>
              <br></br>
              {
                ["formID", "formCalc", "formName", "formImage"].map(function(attribute){
                  var name, instructions;
                  var readonly = false
                  var type = 'text'
                  if(attribute === 'formName'){
                    name = 'Creator name';
                    instructions = 'Enter how you would like the creator to be addressed (e.g. Dr. Kwon)'
                  }else if(attribute === 'formImage'){
                    name = 'Creator image address';
                    instructions = 'If no URL entered, the default image will appear';
                  }else if(attribute === 'formID'){
                    name = 'Calc ID';
                    instructions = 'Unique to each calc';
                    type = 'number'
                    if (self.state.dbid) {
                      readonly = true
                    }

                  }else if(attribute === 'formCalc'){
                    name = 'Calc name';
                    instructions = 'The name that will be referenced in the questions, most commonly the short title'
                  }

                  return (
                    <div>
                      <label>
                        {name}:&nbsp;
                        <input type={type} name={attribute} value={self.state.form[attribute]} onChange={self.changeValue.bind(self)}/>
                      </label><br></br>
                      <div className='small'>{instructions}</div>
                      <br></br>
                    </div>
                  )
                })
              }

            <label>
              Creator bio:&nbsp;
            </label><br></br>
            <textarea name='formBio' type="text" rows="8" cols="75" value={this.state.form.formBio} onChange={this.changeValue.bind(this)}></textarea>
            <br></br><br></br>

            <Questions id='formQuestions' questions={this.state.form.formQuestions} onDelete={this.handleDeleteQuestions.bind(this)}/>
            <br />
            <AddQuestion addQuestion={this.handleAddQuestion.bind(this)}/>

            <br></br>
            {this.renderFormButton()}
            {this.renderDeleteButton()}
            <br></br><br></br>
        </div>
        </div>
        );
    }
}
//
export default App;
