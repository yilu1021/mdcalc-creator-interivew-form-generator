import React, { Component } from 'react';

class AddQuestion extends Component {
  constructor(){
    super();
    //store the new question
    this.state = {
      newQuestion:{},
      value: ''
    }

  }

  handleSubmit(e){
    var newQuestion = {};
    //add the value entered
    newQuestion[this.refs.quest.value] = "";

    //cannot add nothing
    if(this.refs.quest.value === ''){
      alert('A question is required!')
    }else{
      this.setState({
        newQuestion: newQuestion
      }, function(){
        //call addQuestion function in App.js
        this.props.addQuestion(this.state.newQuestion);
      });
      this.setState({
        value: ''
      });
    }
    e.preventDefault();
  }

  change(event){
    this.setState({value: event.target.value});
  }

  render() {

    return (
      <div>
        <div className='minititle'>Add a new question: </div>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <textarea ref='quest' rows="6" cols="75" value={this.state.value} onChange={this.change.bind(this)}/>
          </div>
            <input className='buttonAdd' type="submit" value="Add" />
        </form>
      </div>
    );
  }
}

export default AddQuestion;
