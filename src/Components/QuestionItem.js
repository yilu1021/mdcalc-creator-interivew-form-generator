import React, { Component } from 'react';
import '../App.css';

class QuestionItem extends Component {
  //id is the actual string Question (the key in the formQuestions dict)
  deleteQuestion(id){
    this.props.onDelete(id);
  }

  render() {
    //get current questions in state of Questions.js
    var questions = this.props.question;
    return (
      <div className="Question">
        - {questions} <a href='#' className='buttonDelete' onClick={this.deleteQuestion.bind(this, questions)}>X</a>
      </div>

    );
  }
}

export default QuestionItem;
