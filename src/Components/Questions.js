import React, { Component } from 'react';
import QuestionItem from './QuestionItem';

class Questions extends Component {
  //id is the actual string Question (the key in the formQuestions dict)
  deleteQuestion(id){
    this.props.onDelete(id);
  }

  render() {
    //get current questions in state of App.js
    var questionsArray = Object.keys(this.props.questions);

    let questionItems;
    if(this.props.questions){
      questionItems = questionsArray.map(question => {
        return(
          <div><QuestionItem onDelete={this.deleteQuestion.bind(this)} key={question} question={question} /><br /></div>
        );
      });
    }

    return (
      <div className="Questions">
        <div className='title'>Questions for the creator:</div>
        <hr/>
        <br></br>
          {questionItems}
      </div>
    );
  }
}

export default Questions;
